object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 277
  ClientWidth = 237
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 8
    Width = 62
    Height = 25
    Caption = 'NUM 1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 167
    Top = 7
    Width = 62
    Height = 25
    Caption = 'NUM 2'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 92
    Top = 16
    Width = 51
    Height = 16
    Caption = 'Operator'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object btbtn1: TBitBtn
    Left = 0
    Top = 152
    Width = 75
    Height = 25
    Caption = '1'
    TabOrder = 0
  end
  object btbtn2: TBitBtn
    Left = 81
    Top = 152
    Width = 75
    Height = 25
    Caption = '2'
    TabOrder = 1
    OnClick = btbtn2Click
  end
  object btbtn3: TBitBtn
    Left = 162
    Top = 152
    Width = 75
    Height = 25
    Caption = '3'
    TabOrder = 2
  end
  object btbtn4: TBitBtn
    Left = 0
    Top = 183
    Width = 75
    Height = 25
    Caption = '4'
    TabOrder = 3
  end
  object btbtn5: TBitBtn
    Left = 81
    Top = 183
    Width = 75
    Height = 25
    Caption = '5'
    TabOrder = 4
  end
  object btbtn6: TBitBtn
    Left = 162
    Top = 183
    Width = 75
    Height = 25
    Caption = '6'
    TabOrder = 5
  end
  object btbtn7: TBitBtn
    Left = 0
    Top = 214
    Width = 75
    Height = 25
    Caption = '7'
    TabOrder = 6
  end
  object btbtn8: TBitBtn
    Left = 81
    Top = 214
    Width = 75
    Height = 25
    Caption = '8'
    TabOrder = 7
  end
  object btbtn9: TBitBtn
    Left = 162
    Top = 214
    Width = 75
    Height = 25
    Caption = '9'
    TabOrder = 8
  end
  object btbtnComma: TBitBtn
    Left = 0
    Top = 245
    Width = 75
    Height = 25
    Caption = ','
    TabOrder = 9
  end
  object btbtnZero: TBitBtn
    Left = 81
    Top = 245
    Width = 75
    Height = 25
    Caption = '0'
    TabOrder = 10
  end
  object btbtnEquals: TBitBtn
    Left = 162
    Top = 245
    Width = 75
    Height = 25
    Caption = '='
    TabOrder = 11
  end
  object pnlText: TPanel
    Left = 0
    Top = 80
    Width = 193
    Height = 34
    TabOrder = 12
  end
  object btbtnDiv: TBitBtn
    Left = 122
    Top = 120
    Width = 52
    Height = 26
    Caption = '/'
    TabOrder = 13
    OnClick = btbtnAddClick
  end
  object btbtnMul: TBitBtn
    Left = 180
    Top = 120
    Width = 51
    Height = 26
    Caption = '*'
    TabOrder = 14
    OnClick = btbtnAddClick
  end
  object btbtnSub: TBitBtn
    Left = 65
    Top = 120
    Width = 51
    Height = 26
    Caption = '-'
    TabOrder = 15
    OnClick = btbtnAddClick
  end
  object btbtnAdd: TBitBtn
    Left = 8
    Top = 120
    Width = 51
    Height = 26
    Caption = '+'
    TabOrder = 16
    OnClick = btbtnAddClick
  end
  object edtNum2: TEdit
    Left = 144
    Top = 38
    Width = 93
    Height = 31
    TabOrder = 17
  end
  object edtNum1: TEdit
    Left = 0
    Top = 38
    Width = 93
    Height = 31
    TabOrder = 18
  end
  object edtOpr: TEdit
    Left = 92
    Top = 38
    Width = 53
    Height = 31
    Alignment = taCenter
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -19
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 19
  end
  object btbtnClear: TBitBtn
    Left = 191
    Top = 80
    Width = 38
    Height = 34
    Caption = 'AC'
    TabOrder = 20
  end
end
